ssh-keygen -t rsa -P "" -f $HOME/.ssh/id_rsa

ssh-copy-id -i ~/.ssh/id_rsa.pub localhost

for domain in jt1 dn1 dn2 dn3;
do
  scp -R ~/.ssh hadoop@$domain:~;
  ssh-copy-id -i ~/.ssh/id_rsa.pub $domain;
done;


