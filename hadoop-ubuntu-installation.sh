#!/bin/bash

# Updating repositories
sudo apt-get update
apt-get check

# Java installation
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update

sudo apt-get download oracle-java8-installer
sudo apt-get install oracle-java8-installer

sudo update-alternatives --config java

# Python installation
sudo apt-get install python2.7
sudo wget https://www.python.org/ftp/python/2.7.14/Python-2.7.14.tgz
sudo tar -xzf Python-2.7.14.tgz
cd Python-2.7.14
sudo ./configure --enable-optimizations
sudo make altinstall

# Openssh installation
sudo apt-get install openssh-client
sudo apt-get install openssh-server

# Hadooop user
sudo deluser --remove-home hadoop
sudo adduser hadoop
sudo visudo

# Hadoop installation
sudo mkdir -p /opt/cluster
sudo chmod uog+xwr /opt/cluster
cd /opt/cluster
wget https://www-eu.apache.org/dist/hadoop/common/hadoop-2.8.5/hadoop-2.8.5.tar.gz
tar -xvf hadoop-2.8.5.tar.gz
ln -s hadoop-2.8.5 hadoop

sudo chown -R /opt/cluster hadoop:hadoop
sudo chmod ug+xwr /opt/cluster


